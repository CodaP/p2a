#ifndef _MYSH_H_
#define _MYSH_H_

void listen(char*);
void execute(char *, char **, int, int);
void pwd(void);
void cd(void);
void perform_redirection(int, char**, int*, int*);

#endif /*_MYSH_H_*/
