#!/bin/bash
cd ..
./mysh > tests/output <<here
/bin/pwd
echo hello > out
echo world >> out
ls
cat out
cat out | wc
exit
here


PS1="mysh> " bash --noprofile --norc -i > tests/correct_output 2>&1 <<here
/bin/pwd
echo hello > out
echo world >> out
ls
cat out
cat out | wc
here

diff tests/correct_output tests/output
if [[ $? == 1 ]]
    then
    exit 1
fi

rm -f tests/correct_output tests/output

cd -
