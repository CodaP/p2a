include makefile.inc

EXE=mysh
LIBS=-lreadline -lhistory
LDFLAGS += $(LIBS)
TEST_DIR=tests
SRCS = $(wildcard *.c)
HDRS = $(wildcard *.h)
OBJS = $(SRCS:.c=.o)
HANDIN= ~cs537-1/handin/$(USER)/p2/linux/

all: $(SRCS) $(HDRS) $(EXE) tags

$(EXE): $(OBJS) $(HDRS)

.PHONY: check
check: $(OBJS) $(EXE)
	cd $(TEST_DIR) && $(MAKE)

.PHONY: clean
clean:
	rm -f *.o
	rm -f $(EXE)
	rm -f tags
	cd $(TEST_DIR) && $(MAKE) clean

.PHONY: install
install:
	rsync $(SRCS) $(HANDIN)
	rsync makefile* $(HANDIN)
	rsync $(HDRS) $(HANDIN)
	rsync README.md $(HANDIN)

tags: $(SRCS) $(HDRS)
	ctags $(SRCS) $(HDRS)
