#include "mysh.h"
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <readline/readline.h> /* readline */
#include <readline/history.h> /* using_history, add_history */
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#define EXEC_FAILED 0xCAFEBABE
#define ERROR_STRING "Error!\n"
#define MAX_LINE 1024

/* Pointer to user input string */
char input[MAX_LINE];

/* Pointer to string prefacing each line used to prompt user */
char *prompt_string;

/* Array of strings containing command arguments */
char **exec_argv;

/* Number of command arguments */
int exec_argc;

/* Array to hold ends of pipe */
int pipefd[2];

int main(int argc, char* argv[]) {

    /* pointer to next token */
    char *token;
    /* pointer for re-entrant version of strtok */
    char *save_ptr;
    /* delimiter for pipe */
    const char delim[] = "|";
    /* file descriptor ends for pipe */
    int in,out;
    int return_code, num_children;

    /* Try to get prompt from environmental variable MYSH_PS1 */
    prompt_string = getenv("MYSH_PS1");
    if (prompt_string == NULL) {
        /* Default prompt */
        prompt_string = "mysh>";
    }

    /* Main loop */
	while(true) {
        
        /* Get line */
        printf(prompt_string);
        
        if (fgets(input,MAX_LINE,stdin) == EOF) {
            /* Only happens on EOF (^D) on an empty line */
            printf("\n");
            /* Quit the termial */
            return 0;
        }
        if(strlen(input) == 1){
            continue;
        }

        num_children = 0;

        /* Get first part of pipe (whole command if no pipe) */
        token = strtok_r(input, delim, &save_ptr);
        in = 0;
        out = 1;

        /* Create pipe */
        pipe(pipefd);

        /* Loop pipe until end of tokens */
        while (token != NULL) {

            /* Parse command */
            listen(token);

            /* Check for cd */
            if (strcmp(exec_argv[0],"cd") == 0){
                cd();
                break;
            }
            /* Check for exit */
            else if(strcmp(exec_argv[0],"exit") == 0){
                if(exec_argc == 2){
                    exit(0);
                }else{
                    fprintf(stderr,ERROR_STRING);
                }
            }
            /* Check for pwd */
            else if(strcmp(exec_argv[0],"pwd") == 0){
                pwd();
                break;
            }

            /* Open file descriptors */
            perform_redirection(exec_argc,exec_argv,&in,&out);

            /* if output is to be piped */
            if(strlen(save_ptr) > 0){
                out = pipefd[1];
            }

            /* if is nonempty command */
            if (exec_argc > 1) {
                execute(exec_argv[0],exec_argv,in,out);
                num_children++;
            }

            /* If there is another child in the pipe */
            if(strlen(save_ptr) > 0){
                /* Pipe output to stdin */
                in = pipefd[0];
                out = 1;
            }

            /* Get next token */
            token = strtok_r(NULL, delim,&save_ptr);
        }

        /* After starting all children */
        for(;num_children >0; num_children--){
            /* Close all the pipes */
            close(pipefd[0]);
            close(pipefd[1]);
            /* Wait for children to terminate */
            wait(&return_code);
            /* Check status */
            if (!WIFEXITED(return_code)) {
                if (WIFSIGNALED(return_code)) {
                    printf("Child terminated by signal: %s\n", strsignal(WTERMSIG(return_code)));
                }
            }
            else if(WEXITSTATUS(return_code)){
                fprintf(stderr,ERROR_STRING);
            }
        }
	}
    return 0;
}

/* Convert an input string into a arguments for execvp */
void listen(char * input) {

    char *token;
    const char delim[] = " \n";
    exec_argc = 0;


    /* Parse input, splitting on newline or space into exec_argv */
    token = strtok(input, delim);

    /* Loop until end of tokens */
    while (token != NULL) {

        /* Resize argv to be one bigger*/
        exec_argv = realloc(exec_argv, sizeof(char *) * (++exec_argc));

        /* Append pointer to string */
        exec_argv[exec_argc-1] = token;

        /* Get next token */
        token = strtok(NULL, delim);
    }

    /* Add null to end of array for exec */
    exec_argv = realloc(exec_argv, sizeof(char *) * (++exec_argc));
    exec_argv[exec_argc-1] = NULL;
}

/* Fork to get a new child */
void execute(char *program, char **argv, int in, int out) {
    /* Fork */
	pid_t pid = fork();

    /* if child */
	if (pid == 0) {
        /* If got pipe for stdout or not at all */
        if(pipefd[0] != in && pipefd[0] != out){
            /* Close read end of pipe */
            close(pipefd[0]);
        }
        /* If got pipe for stdin or not at all */
        if(pipefd[1] != in && pipefd[1] != out){
            /* Close write end of pipe */
            close(pipefd[1]);
        }
        /* Replace stdin and stdout with file descriptor arguements */
		dup2(in, 0);
		dup2(out, 1);
		execvp(program, argv); /* will only return if exec faled */
		exit(EXEC_FAILED);
	} else if (pid == -1) {
		perror("Could not fork()");
	} else {
        return;
	}
}

/* Print current working directory */
void pwd(void){
    char * cwd;
    cwd = getcwd(NULL,0);
    if(cwd == NULL){
        perror("");
        return;
    }
    else{
        printf("%s\n",cwd);
        free(cwd);
    }
}

/* Change current working directory */
void cd(void) {
    char *path;
    if (exec_argc == 2) {
        /* just cd */
        path = getenv("HOME");
    } else if(exec_argc == 3) {
        /* cd and an argument */
        path = exec_argv[1];
    } else {
        /* Wrong usage of cd */
        fprintf(stderr,ERROR_STRING);
        return;
    }
        
    if (chdir(path) == -1) {
        //perror("Unable to change directory");
        fprintf(stderr,ERROR_STRING);
    }
}

/* Open the file descriptors needed for redirection */
void perform_redirection(int argc, char *argv[], int * in, int * out) {
    int i;

    /* For arg in command */
    for (i = 1; i < (argc-1); i++) {
        /* if arg is >> */
        if (strcmp(argv[i], ">>") == 0 && (i+1) < argc) {
            /* Open in append mode */
            if((*out = open(exec_argv[i+1], O_WRONLY | O_APPEND | O_CREAT,00660)) == -1){
                perror("");
                exit(1);
            }
            exec_argv[i] = NULL;
            return;
        }
        /* if arg is > */
        if (strcmp(argv[i], ">") == 0 && (i+1) < argc) {
            /* Open in clobber mode */
            if((*out = open(exec_argv[i+1], O_WRONLY | O_CREAT | O_TRUNC,00660)) == -1){
                perror("");
                exit(1);
            }
            exec_argv[i] = NULL;
            return;
        }
        /* if arg is < */
        if (strcmp(argv[i], "<") == 0 && (i+1) < argc) {
            /* Open in read only */
            if((*in = open(exec_argv[i+1], O_RDONLY)) == -1){
                perror("");
                exit(1);
            }
            exec_argv[i] = NULL;
            return;
        }
    }
}

